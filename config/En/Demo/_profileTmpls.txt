Version:5.0
Frame Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|0
6301|0|A25X30X25|0|GRN2|0|
Frame Profile with Gasket (3 cribs)|3 cribs with gasket|any|white|0|0|0|0|0|0|0
6301C|0|A25X30X25|0|
Frame Profile (3 cribs + colored)|3 cribs|any|colored|0|0|0|0|0|0|0
6301K|0|A25X30X25|0|GRN2|0|
Mullion (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|1
6302|0|A30X25X30|0|GRN2|0|GRN1|0|CPT|2|SRM6X60|2|
Mullion with Gasket (3 cribs)|3 cribs with gasket|any|white|0|0|0|0|0|0|1
6302C|0|A30X25X30|0|CPT|2|SRM6X60|2|
Mullion (3 cribs + colored)|3 cribs|any|colored|0|0|0|0|0|0|1
6302K|0|A30X25X30|0|GRN1|0|GRN2|0|CPT|2|SRM6X60|2|
Sash Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|2
6303|0|A25X30X25|0|GRN1|0|GRN2|0|
Sash Profile with Gasket (3 cribs)|3 cribs with gasket|any|white|0|0|0|0|0|0|2
6303C|0|A25X30X25|0|
Sash Profile (3 cribs + colored)|3 cribs|any|colored|0|0|0|0|0|0|2
6303K|0|A25X30X25|0|GRN1|0|GRN2|0|
Door Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|3
6305|0|A50X30X50|0|GRN1|0|GRN2|0|
Door Profile with Gasket (3 cribs)|3 cribs with gasket|any|white|0|0|0|0|0|0|3
6305C|0|A50X30X50|0|
Door Profile (3 cribs + colored)|3 cribs|any|colored|0|0|0|0|0|0|3
6305K|0|A50X30X50|0|GRN1|0|GRN2|0|
Outside Door Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|3
6318|0|A50X30X50|0|GRN1|0|GRN2|0|
Outside Door Profile with Gasket (3 cribs)|3 cribs with gasket|any|white|0|0|0|0|0|0|3
6318C|0|A50X30X50|0|
Outside Door Profile (3 cribs + colored)|3 cribs|any|colored|0|0|0|0|0|0|3
6318K|0|A50X30X50|0|GRN1|0|GRN2|0|
Movable Mullion (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|4
6319|0|GRN2|0|CPM|2|INCS|1|
Movable Mullion with Gasket (3 cribs)|3 cribs with gasket|any|white|0|0|0|0|0|0|4
6319C|0|CPM|2|INCS|1|
Movable Mullion (3 cribs + colored)|3 cribs|any|colored|0|0|0|0|0|0|4
6319K|0|GRN2|0|CPM|2|INCS|1|
Box Profile (3 cribs)|any|any|white|0|0|0|0|0|0|8
6309|0|A50X50|0|
Box Profile (3 cribs + colored)|any|any|colored|0|0|0|0|0|0|8
6309K|0|A50X50|0|
Corner Turning Pipe (3 cribs)|any|any|white|0|0|0|0|0|0|8
6310|0|A50|0|6311|0|6311|0|
Corner Turning Pipe (3 cribs + colored)|any|any|colored|0|0|0|0|0|0|8
6310K|0|A50|0|6311K|0|6311K|0|
Jamb|any|any|any|0|0|0|0|0|0|9
GLF|0|
Mould|any|any|white|0|0|0|0|0|0|11
BND|0|
No Door Sill|any|any|any|0|0|0|0|0|0|10

Door Sill|any|any|any|0|0|0|0|0|0|10
PRG|0|PRPRG|0|
Sliding Frame Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|5
PTODO|0|
Sliding Sash Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|6
PTODO|0|
Sliding Door Profile (3 cribs)|3 cribs|any|white|0|0|0|0|0|0|7
PTODO|0|
